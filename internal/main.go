package main

import (
	"github.com/go-openapi/loads"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/answers"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/answers_comments"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/health_check"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions_comments"
	"go-qna-engine-sbercode-spring-2021/internal/handlers"
	"log"
)

func main() {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatalln(err)
	}

	api := operations.NewAPIAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer func() {
		if err := server.Shutdown(); err != nil {
			log.Fatalln(err)
		}
	}()

	server.Port = 8082

	api.HealthCheckCheckHealthHandler = health_check.CheckHealthHandlerFunc(handlers.HealthCheckHandler)

	api.QuestionsAddQuestionHandler = questions.AddQuestionHandlerFunc(handlers.AddQuestionHandler)
	api.QuestionsGetQuestionByIDHandler = questions.GetQuestionByIDHandlerFunc(handlers.GetQuestionByIDHandler)
	api.QuestionsEditQuestionByIDHandler = questions.EditQuestionByIDHandlerFunc(handlers.EditQuestionByIDHandler)
	api.QuestionsGetQuestionByFiltersHandler = questions.GetQuestionByFiltersHandlerFunc(handlers.GetQuestionsByFilters)
	api.QuestionsDeleteQuestionByIDHandler = questions.DeleteQuestionByIDHandlerFunc(handlers.DeleteQuestionByIDHandler)

	api.AnswersAddAnswerHandler = answers.AddAnswerHandlerFunc(handlers.AddAnswerHandler)
	api.AnswersGetAnswersByIDHandler = answers.GetAnswersByIDHandlerFunc(handlers.GetAnswerByIDHandler)
	api.AnswersEditAnswerByIDHandler = answers.EditAnswerByIDHandlerFunc(handlers.EditAnswerByIDHandler)
	api.AnswersDeleteAnswerByIDHandler = answers.DeleteAnswerByIDHandlerFunc(handlers.DeleteAnswerByIDHandler)

	api.AnswersCommentsGetAnswerCommentByIDHandler = answers_comments.GetAnswerCommentByIDHandlerFunc(handlers.GetAnswerCommentByIDHandler)
	api.AnswersCommentsAddAnswerCommentHandler = answers_comments.AddAnswerCommentHandlerFunc(handlers.AddAnswerCommnetHandler)

	api.QuestionsCommentsAddQuestionCommentHandler = questions_comments.AddQuestionCommentHandlerFunc(handlers.AddQuestionCommnetHandler)
	api.QuestionsCommentsGetQuestionCommentByIDHandler = questions_comments.GetQuestionCommentByIDHandlerFunc(handlers.GetQuestionCommentByIDHandler)


	if err := server.Serve(); err != nil {
		log.Fatalln(err)
	}
}
