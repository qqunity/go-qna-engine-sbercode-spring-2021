package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector/methods"
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/answers_comments"
	"log"
)

func GetAnswerCommentByIDHandler(params answers_comments.GetAnswerCommentByIDParams) middleware.Responder {
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	answerComment, err := methods.GetAnswerCommentById(connector.DBConnector(true), params.AnswerCommentID, true)
	if err != nil {
		log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
		return answers_comments.NewGetAnswerCommentByIDBadRequest().WithPayload(
			&models.BadRequestInfo{
				ErrorCode: 400,
				Status:    false,
			},
		)
	}
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Body: |%v|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, *answerComment)
	return answers_comments.NewGetAnswerCommentByIDOK().WithPayload(
		&models.AnswerCommentInfo{
				ID:         answerComment.ID,
				AnswerID: answerComment.AnswerId,
				Content:    answerComment.Content,
				CreatedBy:  answerComment.CreatedBy,
				Rating:     answerComment.Rating,
				CreatedAt:  answerComment.CreatedAt.String(),
				UpdatedAt:  answerComment.UpdatedAt.String(),
		},
	)
}