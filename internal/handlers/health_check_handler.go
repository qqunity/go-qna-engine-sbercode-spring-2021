package handlers

import (
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/health_check"
	"log"

	"github.com/go-openapi/runtime/middleware"
)

func HealthCheckHandler(params health_check.CheckHealthParams) middleware.Responder {
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header,params.HTTPRequest.RequestURI)

	return health_check.NewCheckHealthOK().WithPayload(
		&models.SuccessRequestInfo{
			ErrorCode: 0,
			Status:    true,
		},
	)
}
