package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector/methods"
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions"
	"log"
)

func EditQuestionByIDHandler(params questions.EditQuestionByIDParams) middleware.Responder {
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	err := methods.EditQuestionByIdQuestion(connector.DBConnector(true), params.QuestionID, params, true)
	if err != nil {
		log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
		return questions.NewEditQuestionByIDBadRequest().WithPayload(
			&models.BadRequestInfo{
				ErrorCode: 400,
				Status:    false,
			},
		)
	}
	return questions.NewEditQuestionByIDOK().WithPayload(
		&models.SuccessRequestInfo{
			ErrorCode: 0,
			Status:    true,
		},
	)
}
