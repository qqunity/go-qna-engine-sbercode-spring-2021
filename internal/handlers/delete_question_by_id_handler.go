package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector/methods"
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions"
	"log"
)

func DeleteQuestionByIDHandler(params questions.DeleteQuestionByIDParams) middleware.Responder {
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	err := methods.DeleteQuestionByID(connector.DBConnector(true), params.QuestionID, true)
	if err != nil {
		log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
		return questions.NewDeleteQuestionByIDBadRequest().WithPayload(
			&models.BadRequestInfo{
				ErrorCode: 400,
				Status:    false,
			},
		)
	}
	return questions.NewDeleteQuestionByIDOK().WithPayload(
		&models.SuccessRequestInfo{
			ErrorCode: 0,
			Status:    true,
		},
	)
}