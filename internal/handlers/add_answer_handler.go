package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector/methods"
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/answers"
	"log"
)

func AddAnswerHandler(params answers.AddAnswerParams) middleware.Responder {
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	answer, err := methods.AddAnswer(connector.DBConnector(true), params, true)
	if err != nil {
		log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
		return answers.NewAddAnswerBadRequest().WithPayload(
			&models.BadRequestInfo{
				ErrorCode: 400,
				Status:    false,
			},
		)
	}
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Sucsess", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	return answers.NewAddAnswerOK().WithPayload(
		&models.SuccessRequestWithAnswerInfo{
			Status:    true,
			ErrorCode: 0,
			Answer: &models.AnswerInfo{
				ID:         answer.ID,
				QuestionID: answer.QuestionId,
				Content:    answer.Content,
				CreatedBy:  answer.CreatedBy,
				Rating:     answer.Rating,
				IsTrusted:  answer.IsTrusted,
				CreatedAt:  answer.CreatedAt.String(),
				UpdatedAt:  answer.UpdatedAt.String(),
			},
		},
	)
}
