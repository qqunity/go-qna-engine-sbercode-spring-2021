package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector/methods"
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions"
	"log"
)

func GetQuestionsByFilters(params questions.GetQuestionByFiltersParams) middleware.Responder {

	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	if params.FilterData.Substring != "" {
		questionsInfo, err := methods.GetQuestionsBySubstringHeader(connector.DBConnector(true), params.FilterData.Substring, params.FilterData.OrderByRating, true)
		if err != nil {
			log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
			return questions.NewGetQuestionByFiltersBadRequest().WithPayload(
				&models.BadRequestInfo{
					ErrorCode: 400,
					Status:    false,
				},
			)
		}
		return questions.NewGetQuestionByFiltersOK().WithPayload(
			questionsInfo,
		)
	} else if params.FilterData.Tags != "" {
		questionsInfo, err := methods.GetQuestionsByTags(connector.DBConnector(true), params.FilterData.Tags, params.FilterData.OrderByRating, true)
		if err != nil {
			log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
			return questions.NewGetQuestionByFiltersBadRequest().WithPayload(
				&models.BadRequestInfo{
					ErrorCode: 400,
					Status:    false,
				},
			)
		}
		return questions.NewGetQuestionByFiltersOK().WithPayload(
			questionsInfo,
		)
	} else if params.FilterData.CreatedBy != "" {
		questionsInfo, err := methods.GetQuestionsByUserIdentifier(connector.DBConnector(true), params.FilterData.CreatedBy, params.FilterData.OrderByRating, true)
		if err != nil {
			log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
			return questions.NewGetQuestionByFiltersBadRequest().WithPayload(
				&models.BadRequestInfo{
					ErrorCode: 400,
					Status:    false,
				},
			)
		}
		return questions.NewGetQuestionByFiltersOK().WithPayload(
			questionsInfo,
		)
	} else {
		questionsInfo, err := methods.GetAllQuestions(connector.DBConnector(true), params.FilterData.OrderByRating, true)
		if err != nil {
			log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
			return questions.NewGetQuestionByFiltersBadRequest().WithPayload(
				&models.BadRequestInfo{
					ErrorCode: 400,
					Status:    false,
				},
			)
		}
		return questions.NewGetQuestionByFiltersOK().WithPayload(
			questionsInfo,
		)
	}
}