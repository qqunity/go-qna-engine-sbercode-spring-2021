package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector/methods"
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions"
	"log"
)

func AddQuestionHandler(params questions.AddQuestionParams) middleware.Responder {
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	question, err := methods.AddQuestion(connector.DBConnector(true), params, true)
	if err != nil {
		log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
		return questions.NewAddQuestionBadRequest().WithPayload(
			&models.BadRequestInfo{
				ErrorCode: 400,
				Status:    false,
			},
		)
	}
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Sucsess", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	return questions.NewAddQuestionOK().WithPayload(
		&models.SuccessRequestWithQuestionInfo{
			ErrorCode: 0,
			Status: false,
			Question: &models.QuestionInfo{
				ID: question.ID,
				CreatedBy: question.CreatedBy,
				Header: question.Header,
				Content: question.Content,
				Tags: question.Tags,
				IsSolved: question.IsSolved,
				Rating: question.Rating,
				Priority: question.Priority,
				CreatedAt: question.CreatedAt.String(),
				UpdatedAt: question.UpdatedAt.String(),
			},
		},
	)
}
