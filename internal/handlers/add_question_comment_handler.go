package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector/methods"
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions_comments"
	"log"
)

func AddQuestionCommnetHandler(params questions_comments.AddQuestionCommentParams) middleware.Responder {
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	questionComment, err := methods.AddQuestionComment(connector.DBConnector(true), params, true)
	if err != nil {
		log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
		return questions_comments.NewAddQuestionCommentBadRequest().WithPayload(
			&models.BadRequestInfo{
				ErrorCode: 400,
				Status:    false,
			},
		)
	}
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Sucsess", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	return questions_comments.NewAddQuestionCommentOK().WithPayload(
		&models.SuccessRequestWithQuestionCommentInfo{
			Status:    true,
			ErrorCode: 0,
			Answer: &models.QuestionCommentInfo{
				ID:         questionComment.ID,
				QuestionID: questionComment.QuestionId,
				Content:    questionComment.Content,
				CreatedBy:  questionComment.CreatedBy,
				Rating:     questionComment.Rating,
				CreatedAt:  questionComment.CreatedAt.String(),
				UpdatedAt:  questionComment.UpdatedAt.String(),
			},
		},
	)
}
