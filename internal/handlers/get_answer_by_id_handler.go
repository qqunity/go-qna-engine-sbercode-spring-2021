package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector/methods"
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/answers"
	"log"
)

func GetAnswerByIDHandler(params answers.GetAnswersByIDParams) middleware.Responder {
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	answer, err := methods.GetAnswerById(connector.DBConnector(true), params.AnswerID, true)
	if err != nil {
		log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
		return answers.NewGetAnswersByIDBadRequest().WithPayload(
			&models.BadRequestInfo{
				ErrorCode: 400,
				Status:    false,
			},
		)
	}
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Body: |%v|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, *answer)
	return answers.NewGetAnswersByIDOK().WithPayload(
		&models.AnswerInfo{
			ID:         answer.ID,
			QuestionID: answer.QuestionId,
			Content:    answer.Content,
			CreatedBy:  answer.CreatedBy,
			Rating:     answer.Rating,
			IsTrusted:  answer.IsTrusted,
			CreatedAt:  answer.CreatedAt.String(),
			UpdatedAt:  answer.UpdatedAt.String(),
			Comments:   answer.AnswerCommentsWeb,
		},
	)
}
