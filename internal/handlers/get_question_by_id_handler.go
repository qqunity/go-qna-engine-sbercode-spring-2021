package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector/methods"
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions"
	"log"
)

func GetQuestionByIDHandler(params questions.GetQuestionByIDParams) middleware.Responder {
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	question, err := methods.GetQuestionById(connector.DBConnector(true), params.QuestionID, true, true)
	if err != nil {
		log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
		return questions.NewGetQuestionByIDBadRequest().WithPayload(
			&models.BadRequestInfo{
				ErrorCode: 400,
				Status:    false,
			},
		)
	}
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Body: |%v|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, *question)
	return questions.NewGetQuestionByIDOK().WithPayload(
		&models.QuestionInfo{
			ID: question.ID,
			CreatedBy: question.CreatedBy,
			Header: question.Header,
			Content: question.Content,
			Tags: question.Tags,
			Priority: question.Priority,
			IsSolved: question.IsSolved,
			Rating: question.Rating,
			Answers: question.AnswersWeb,
			Comments: question.QuestionCommentsWeb,
			CreatedAt: question.CreatedAt.String(),
			UpdatedAt: question.UpdatedAt.String(),
		},
		)
}
