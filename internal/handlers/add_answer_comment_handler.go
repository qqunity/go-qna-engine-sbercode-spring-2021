package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector/methods"
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/answers_comments"
	"log"
)

func AddAnswerCommnetHandler(params answers_comments.AddAnswerCommentParams) middleware.Responder {
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	answerComment, err := methods.AddAnswerComment(connector.DBConnector(true), params, true)
	if err != nil {
		log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
		return answers_comments.NewAddAnswerCommentBadRequest().WithPayload(
			&models.BadRequestInfo{
				ErrorCode: 400,
				Status:    false,
			},
		)
	}
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Sucsess", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	return answers_comments.NewAddAnswerCommentOK().WithPayload(
		&models.SuccessRequestWithAnswerCommentInfo{
			Status:    true,
			ErrorCode: 0,
			Answer: &models.AnswerCommentInfo{
				ID:         answerComment.ID,
				AnswerID: answerComment.AnswerId,
				Content:    answerComment.Content,
				CreatedBy:  answerComment.CreatedBy,
				Rating:     answerComment.Rating,
				CreatedAt:  answerComment.CreatedAt.String(),
				UpdatedAt:  answerComment.UpdatedAt.String(),
			},
		},
	)
}