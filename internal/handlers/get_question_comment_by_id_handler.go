package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector"
	"go-qna-engine-sbercode-spring-2021/internal/database/connector/methods"
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions_comments"
	"log"
)

func GetQuestionCommentByIDHandler(params questions_comments.GetQuestionCommentByIDParams) middleware.Responder {
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI)
	questionComment, err := methods.GetQuestionCommentById(connector.DBConnector(true), params.QuestionCommentID, true)
	if err != nil {
		log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Error: |%s|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, err.Error())
		return questions_comments.NewGetQuestionCommentByIDBadRequest().WithPayload(
			&models.BadRequestInfo{
				ErrorCode: 400,
				Status:    false,
			},
		)
	}
	log.Printf("REQUEST: Method |%s| \t Headers: |%s| \t Route: |%s| \t Body: |%v|", params.HTTPRequest.Method, params.HTTPRequest.Header, params.HTTPRequest.RequestURI, *questionComment)
	return questions_comments.NewGetQuestionCommentByIDOK().WithPayload(
		&models.QuestionCommentInfo{
			ID:         questionComment.ID,
			QuestionID: questionComment.QuestionId,
			Content:    questionComment.Content,
			CreatedBy:  questionComment.CreatedBy,
			Rating:     questionComment.Rating,
			CreatedAt:  questionComment.CreatedAt.String(),
			UpdatedAt:  questionComment.UpdatedAt.String(),
		},
	)
}
