package utils

func GetInterceptionSliceWithTags(firstSlice []string, secondSlice []string)  []string{
	var resultSlice []string
	checkMap := map[string]struct{}{}

	for _, addr := range firstSlice {
		checkMap[addr] = struct{}{}
	}
	for _, addr := range secondSlice {
		if _, ok := checkMap[addr]; ok {
			resultSlice = append(resultSlice, addr)
		}
	}
	return resultSlice
}
