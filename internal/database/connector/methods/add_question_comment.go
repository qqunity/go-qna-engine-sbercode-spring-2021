package methods

import (
	"github.com/go-pg/pg/v10"
	"go-qna-engine-sbercode-spring-2021/internal/database/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions_comments"
)

func AddQuestionComment(db *pg.DB, questionParams questions_comments.AddQuestionCommentParams, shouldCloseDB bool) (*models.QuestionComment, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	id := ID{}
	_, err := db.Query(&id, "select last_value + 1 id from question_comment_id_seq;")
	if err != nil {
		return nil, err
	}
	_, err = db.Model(&models.QuestionComment{
		QuestionId: questionParams.QuestionCommentData.QuestionID,
		Content: questionParams.QuestionCommentData.Content,
		CreatedBy: questionParams.QuestionCommentData.CreatedBy,
		Rating: questionParams.QuestionCommentData.Rating,
	}).
		Insert()
	if err != nil {
		return nil, err
	}
	questionComment, err := GetQuestionCommentById(db, id.ID, false)
	if err != nil {
		return nil, err
	}
	return questionComment, nil
}
