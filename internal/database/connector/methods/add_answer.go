package methods

import (
	"github.com/go-pg/pg/v10"
	"go-qna-engine-sbercode-spring-2021/internal/database/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/answers"
)

func AddAnswer(db *pg.DB, answerParams answers.AddAnswerParams, shouldCloseDB bool) (*models.Answer, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	id := ID{}
	_, err := db.Query(&id, "select last_value + 1 id from answer_id_seq;")
	if err != nil {
		return nil, err
	}
	_, err = db.Model(&models.Answer{
		QuestionId: answerParams.AnswerData.QuestionID,
		Content: answerParams.AnswerData.Content,
		CreatedBy: answerParams.AnswerData.CreatedBy,
		Rating: answerParams.AnswerData.Rating,
		IsTrusted: answerParams.AnswerData.IsTrusted,
	}).
		Insert()
	if err != nil {
		return nil, err
	}
	answer, err := GetAnswerById(db, id.ID, false)
	if err != nil {
		return nil, err
	}
	return answer, nil
}
