package methods

import (
	"github.com/go-pg/pg/v10"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions"
	"time"
)

func EditQuestionByIdQuestion(db *pg.DB, questionID int64, questionParams questions.EditQuestionByIDParams, shouldCloseDB bool) error {
	if shouldCloseDB {
		defer db.Close()
	}
	question, err := GetQuestionById(db, questionID, false, false)
	if err != nil {
		return err
	}
	_, err = db.Model(question).
		Set("header = ?", func() string { if questionParams.QuestionData.Header != "" { return questionParams.QuestionData.Header } else { return question.Header } }()).
		Set("content = ?", func() []byte { if questionParams.QuestionData.Content != nil { return questionParams.QuestionData.Content } else { return question.Content } }()).
		Set("tags = ?", func() string { if questionParams.QuestionData.Tags != "" { return questionParams.QuestionData.Tags } else { return question.Tags } }()).
		Set("rating = ?", func() int64 { if questionParams.QuestionData.Rating != 0 { return questionParams.QuestionData.Rating } else { return question.Rating } }()).
		Set("priority = ?", func() uint64 { if questionParams.QuestionData.Priority != 0 { return questionParams.QuestionData.Priority } else { return question.Priority } }()).
		Set("is_solved = ?", questionParams.QuestionData.IsSolved).
		Set("updated_at = ?", time.Now()).
		Where("id = ?", question.ID).
		Update()

	if err != nil {
		return err
	}
	return nil
}