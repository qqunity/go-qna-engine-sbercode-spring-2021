package methods

import (
	"github.com/go-pg/pg/v10"
	"go-qna-engine-sbercode-spring-2021/internal/database/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/answers_comments"
)

func AddAnswerComment(db *pg.DB, answerParams answers_comments.AddAnswerCommentParams, shouldCloseDB bool) (*models.AnswerComment, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	id := ID{}
	_, err := db.Query(&id, "select last_value + 1 id from answer_comment_id_seq;")
	if err != nil {
		return nil, err
	}
	_, err = db.Model(&models.AnswerComment{
		AnswerId: answerParams.AnswerCommentData.AnswerID,
		Content: answerParams.AnswerCommentData.Content,
		CreatedBy: answerParams.AnswerCommentData.CreatedBy,
		Rating: answerParams.AnswerCommentData.Rating,
	}).
		Insert()
	if err != nil {
		return nil, err
	}
	answerComment, err := GetAnswerCommentById(db, id.ID, false)
	if err != nil {
		return nil, err
	}
	return answerComment, nil
}