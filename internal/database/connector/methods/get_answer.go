package methods

import (
	"github.com/go-pg/pg/v10"
	"go-qna-engine-sbercode-spring-2021/internal/database/models"
	webModels "go-qna-engine-sbercode-spring-2021/internal/generated/models"
)

func GetAnswerById(db *pg.DB, answerId int64, shouldCloseDB bool) (*models.Answer, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	answer := new(models.Answer)
	err := db.Model(answer).
		Where("id = ?", answerId).
		Relation("AnswerComments").
		Select()
	var answerComments []*webModels.AnswerCommentInfo
	for i := range answer.AnswerComments {
		answerComments = append(answerComments, &webModels.AnswerCommentInfo{
			ID: answer.AnswerComments[i].ID,
			AnswerID: answer.AnswerComments[i].AnswerId,
			Content: answer.AnswerComments[i].Content,
			CreatedBy: answer.AnswerComments[i].CreatedBy,
			Rating: answer.AnswerComments[i].Rating,
			CreatedAt: answer.AnswerComments[i].CreatedAt.String(),
			UpdatedAt: answer.AnswerComments[i].UpdatedAt.String(),
		})
	}
	answer.AnswerCommentsWeb = answerComments
	if err != nil {
		return nil, err
	}
	return answer, nil
}
