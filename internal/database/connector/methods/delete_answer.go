package methods

import (
	"github.com/go-pg/pg/v10"
	"go-qna-engine-sbercode-spring-2021/internal/database/models"
)

func DeleteAnswerByID(db *pg.DB, answerId int64, shouldCloseDB bool) error {
	if shouldCloseDB {
		defer db.Close()
	}
	_, err := GetAnswerById(db, answerId, false)
	if err != nil {
		return err
	}
	var answer *models.Answer
	_, err = db.Model(answer).
		Where("id = ?", answerId).
		Delete()
	if err != nil {
		return err
	}
	return nil
}