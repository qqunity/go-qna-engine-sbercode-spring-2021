package methods

import (
	"github.com/go-pg/pg/v10"
	"go-qna-engine-sbercode-spring-2021/internal/database/models"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/questions"
)

type ID struct {
	ID int64 `pg:"id"`
}

func AddQuestion(db *pg.DB, questionParams questions.AddQuestionParams, shouldCloseDB bool) (*models.Question, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	id := ID{}
	_, err := db.Query(&id, "select last_value + 1 id from question_id_seq;")
	if err != nil {
		return nil, err
	}
	_, err = db.Model(&models.Question{
		CreatedBy: questionParams.QuestionData.CreatedBy,
		Header:    questionParams.QuestionData.Header,
		Content:   questionParams.QuestionData.Content,
		Tags:      questionParams.QuestionData.Tags,
		IsSolved:  questionParams.QuestionData.IsSolved,
		Priority:  questionParams.QuestionData.Priority,
		Rating:    questionParams.QuestionData.Rating,
	}).
		Insert()
	if err != nil {
		return nil, err
	}
	question, err := GetQuestionById(db, id.ID, false, false)
	if err != nil {
		return nil, err
	}
	return question, nil
}
