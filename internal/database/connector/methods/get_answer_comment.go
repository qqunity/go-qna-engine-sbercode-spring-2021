package methods

import (
	"github.com/go-pg/pg/v10"
	"go-qna-engine-sbercode-spring-2021/internal/database/models"
)

func GetAnswerCommentById(db *pg.DB, answerCommentId int64, shouldCloseDB bool) (*models.AnswerComment, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	answerComment := new(models.AnswerComment)
	err := db.Model(answerComment).
		Where("id = ?", answerCommentId).
		Select()
	if err != nil {
		return nil, err
	}
	return answerComment, nil
}
