package methods

import (
	"github.com/go-pg/pg/v10"
)

func DeleteQuestionByID(db *pg.DB, questionId int64, shouldCloseDB bool) error {
	if shouldCloseDB {
		defer db.Close()
	}
	question, err := GetQuestionById(db, questionId, true, false)
	if question != nil {
		for i := range question.Answers {
			_ = DeleteAnswerByID(db, int64(question.Answers[i].ID), false)
		}
	}
	if err != nil {
		return err
	}
	_, err = db.Model(question).
		Where("id = ?", questionId).
		Delete()
	if err != nil {
		return err
	}
	return nil
}