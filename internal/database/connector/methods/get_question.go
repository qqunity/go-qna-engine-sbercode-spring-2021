package methods

import (
	"fmt"
	"github.com/go-pg/pg/v10"
	"go-qna-engine-sbercode-spring-2021/internal/database/models"
	webModels "go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"go-qna-engine-sbercode-spring-2021/internal/utils"
	"strings"
)

func GetQuestionById(db *pg.DB, questionId int64, isAddedAnswers bool, shouldCloseDB bool) (*models.Question, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	question := new(models.Question)
	var err error
	if isAddedAnswers {
		err = db.Model(question).
			Where("id = ?", questionId).
			Relation("Answers").
			Relation("QuestionComments").
			Select()
		var answersInfo []*webModels.AnswerInfo
		var commentsInfo []*webModels.QuestionCommentInfo
		for i := range question.Answers{
			buffAnswer, _ := GetAnswerById(db, int64(question.Answers[i].ID), false)
			answersInfo = append(answersInfo, &webModels.AnswerInfo{
				ID: question.Answers[i].ID,
				QuestionID: question.Answers[i].QuestionId,
				Content: question.Answers[i].Content,
				CreatedBy: question.Answers[i].CreatedBy,
				Rating: question.Answers[i].Rating,
				IsTrusted: question.Answers[i].IsTrusted,
				Comments: buffAnswer.AnswerCommentsWeb,
				CreatedAt: question.Answers[i].CreatedAt.String(),
				UpdatedAt: question.Answers[i].UpdatedAt.String(),
			})
		}
		for i := range question.QuestionComments {
			commentsInfo = append(commentsInfo, &webModels.QuestionCommentInfo{
				ID: question.QuestionComments[i].ID,
				QuestionID: question.QuestionComments[i].QuestionId,
				Content: question.QuestionComments[i].Content,
				CreatedBy: question.QuestionComments[i].CreatedBy,
				Rating: question.QuestionComments[i].Rating,
				CreatedAt: question.QuestionComments[i].CreatedAt.String(),
				UpdatedAt: question.QuestionComments[i].UpdatedAt.String(),
			})
		}
		question.AnswersWeb = answersInfo
		question.QuestionCommentsWeb = commentsInfo
	} else {
		err = db.Model(question).
			Where("id = ?", questionId).
			Select()
		question.AnswersWeb = nil
	}

	if err != nil {
		return nil, err
	}
	return question, nil
}

func GetQuestionsBySubstringHeader(db *pg.DB, substring string, orderByRating bool, shouldCloseDB bool) ([]*webModels.QuestionInfo, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	var questions []*models.Question
	var err error
	if orderByRating {
		_, err = db.Query(&questions, fmt.Sprintf("SELECT * FROM question WHERE header LIKE '%%%s%%' ORDER BY priority DESC, rating DESC;", substring))
	} else {
		_, err = db.Query(&questions, fmt.Sprintf("SELECT * FROM question WHERE header LIKE '%%%s%%' ORDER BY priority DESC;", substring))
	}
	if err != nil {
		return nil, err
	}
	var questionsModels []*webModels.QuestionInfo
	for i := range questions {
		questionsModels = append(questionsModels, &webModels.QuestionInfo{
			ID:        questions[i].ID,
			CreatedBy: questions[i].CreatedBy,
			Header:    questions[i].Header,
			Content:   questions[i].Content,
			Tags:      questions[i].Tags,
			Rating:    questions[i].Rating,
			Priority:  questions[i].Priority,
			IsSolved:  questions[i].IsSolved,
			CreatedAt: questions[i].CreatedAt.String(),
			UpdatedAt: questions[i].UpdatedAt.String(),
		})
	}
	return questionsModels, err
}

func GetQuestionsByTags(db *pg.DB, tags string, orderByRating bool, shouldCloseDB bool) ([]*webModels.QuestionInfo, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	var questions []*models.Question
	var err error
	if orderByRating {
		_, err = db.Query(&questions, "SELECT * FROM question ORDER BY priority DESC, rating DESC;")
	} else {
		_, err = db.Query(&questions, "SELECT * FROM question ORDER BY priority DESC;")
	}
	if err != nil {
		return nil, err
	}
	var questionsModels []*webModels.QuestionInfo
	for i := range questions {
		if len(utils.GetInterceptionSliceWithTags(strings.Split(questions[i].Tags, ";"), strings.Split(tags, ";"))) != 0 {
			questionsModels = append(questionsModels, &webModels.QuestionInfo{
				ID:        questions[i].ID,
				CreatedBy: questions[i].CreatedBy,
				Header:    questions[i].Header,
				Content:   questions[i].Content,
				Tags:      questions[i].Tags,
				Priority:  questions[i].Priority,
				Rating:    questions[i].Rating,
				IsSolved:  questions[i].IsSolved,
				CreatedAt: questions[i].CreatedAt.String(),
				UpdatedAt: questions[i].UpdatedAt.String(),
			})
		}
	}
	return questionsModels, err
}

func GetAllQuestions(db *pg.DB, orderByRating bool, shouldCloseDB bool) ([]*webModels.QuestionInfo, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	var questions []*models.Question
	var err error
	if orderByRating {
		_, err = db.Query(&questions, "SELECT * FROM question ORDER BY priority DESC, rating DESC;")
	} else {
		_, err = db.Query(&questions, "SELECT * FROM question ORDER BY priority DESC;")
	}
	if err != nil {
		return nil, err
	}
	var questionsModels []*webModels.QuestionInfo
	for i := range questions {
		questionsModels = append(questionsModels, &webModels.QuestionInfo{
			ID:        questions[i].ID,
			CreatedBy: questions[i].CreatedBy,
			Header:    questions[i].Header,
			Content:   questions[i].Content,
			Tags:      questions[i].Tags,
			Rating:    questions[i].Rating,
			Priority:  questions[i].Priority,
			IsSolved:  questions[i].IsSolved,
			CreatedAt: questions[i].CreatedAt.String(),
			UpdatedAt: questions[i].UpdatedAt.String(),
		})
	}
	return questionsModels, err
}

func GetQuestionsByUserIdentifier(db *pg.DB, userIdentifier string, orderByRating bool, shouldCloseDB bool) ([]*webModels.QuestionInfo, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	var questions []*models.Question
	var err error
	if orderByRating {
		_, err = db.Query(&questions, fmt.Sprintf("SELECT * FROM question WHERE created_by = '%s' ORDER BY priority DESC, rating DESC;", userIdentifier))
	} else {
		_, err = db.Query(&questions, fmt.Sprintf("SELECT * FROM question WHERE created_by = '%s' ORDER BY priority DESC;", userIdentifier))
	}
	if err != nil {
		return nil, err
	}
	var questionsModels []*webModels.QuestionInfo
	for i := range questions {
		questionsModels = append(questionsModels, &webModels.QuestionInfo{
			ID:        questions[i].ID,
			CreatedBy: questions[i].CreatedBy,
			Header:    questions[i].Header,
			Content:   questions[i].Content,
			Tags:      questions[i].Tags,
			Rating:    questions[i].Rating,
			Priority:  questions[i].Priority,
			IsSolved:  questions[i].IsSolved,
			CreatedAt: questions[i].CreatedAt.String(),
			UpdatedAt: questions[i].UpdatedAt.String(),
		})
	}
	return questionsModels, err
}
