package methods

import (
	"github.com/go-pg/pg/v10"
	"go-qna-engine-sbercode-spring-2021/internal/generated/restapi/operations/answers"
	"time"
)

func EditAnswerByIdQuestion(db *pg.DB, answerId int64, answerParams answers.EditAnswerByIDParams, shouldCloseDB bool) error {
	if shouldCloseDB {
		defer db.Close()
	}
	answer, err := GetAnswerById(db, answerId, false)
	if err != nil {
		return err
	}
	_, err = db.Model(answer).
		Set("content = ?", func() []byte { if answerParams.AnswerData.Content != nil { return answerParams.AnswerData.Content } else { return answer.Content } }()).
		Set("created_by = ?", func() string { if answerParams.AnswerData.CreatedAt != "" { return answerParams.AnswerData.CreatedAt } else { return answer.CreatedBy } }()).
		Set("rating = ?", func() int64 { if answerParams.AnswerData.Rating != 0 { return answerParams.AnswerData.Rating } else { return answer.Rating } }()).
		Set("is_trusted = ?", answerParams.AnswerData.IsTrusted).
		Set("updated_at = ?", time.Now()).
		Where("id = ?", answer.ID).
		Update()

	if err != nil {
		return err
	}
	return nil
}