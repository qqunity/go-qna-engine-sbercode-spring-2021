package methods

import (
	"github.com/go-pg/pg/v10"
	"go-qna-engine-sbercode-spring-2021/internal/database/models"
)

func GetQuestionCommentById(db *pg.DB, questionCommentId int64, shouldCloseDB bool) (*models.QuestionComment, error) {
	if shouldCloseDB {
		defer db.Close()
	}
	questionComment := new(models.QuestionComment)
	err := db.Model(questionComment).
		Where("id = ?", questionCommentId).
		Select()
	if err != nil {
		return nil, err
	}
	return questionComment, nil
}
