package connector

import (
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	myModels "go-qna-engine-sbercode-spring-2021/internal/database/models"
)

func DBConnector(isDBCreated bool) *pg.DB {
	db := pg.Connect(&pg.Options{
		Addr:     "82.146.61.94:5432",
		User:     "root",
		Password: "root",
		Database: "qnaengine",
	})
	if !isDBCreated {
		err := createSchema(db)
		if err != nil {
			panic(fmt.Errorf("FAIL. Fatal error with db connector: %s", err))
		}
	}
	return db
}

func createSchema(db *pg.DB) error {
	models := []interface{}{
		(*myModels.AnswerComment)(nil),
		(*myModels.QuestionComment)(nil),
	}

	for _, model := range models {
		err := db.Model(model).CreateTable(&orm.CreateTableOptions{
			Temp: false,
		})
		if err != nil {
			return err
		}
	}
	return nil
}
