package models

import (
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"time"
)

type Answer struct {
	tableName         struct{}                    `pg:"answer"`
	ID                uint64                      `pg:",pk" json:"id"`
	QuestionId        uint64                      `json:"question_id"`
	Content           []byte                      `json:"content"`
	CreatedBy         string                      `json:"created_by"`
	Rating            int64                       `json:"rating"`
	IsTrusted         bool                        `pg:"default:false" json:"is_trusted"`
	CreatedAt         time.Time                   `pg:"default:now()" json:"created_at"`
	UpdatedAt         time.Time                   `pg:"default:now()" json:"updated_at"`
	AnswerComments    []*AnswerComment            `pg:"rel:has-many" json:"-"`
	AnswerCommentsWeb []*models.AnswerCommentInfo `pg:"-" json:"-"`
}
