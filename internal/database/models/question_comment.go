package models

import "time"

type QuestionComment struct {
	tableName  struct{}  `pg:"question_comment"`
	ID         uint64    `pg:",pk" json:"id"`
	QuestionId uint64    `json:"question_id"`
	Content    string    `json:"content"`
	CreatedBy  string    `json:"created_by"`
	Rating     int64     `json:"rating"`
	CreatedAt  time.Time `pg:"default:now()" json:"created_at"`
	UpdatedAt  time.Time `pg:"default:now()" json:"updated_at"`
}
