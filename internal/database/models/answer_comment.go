package models

import "time"

type AnswerComment struct {
	tableName struct{}  `pg:"answer_comment"`
	ID        uint64    `pg:",pk" json:"id"`
	AnswerId  uint64    `json:"answer_id"`
	Content   string    `json:"content"`
	CreatedBy string    `json:"created_by"`
	Rating    int64     `json:"rating"`
	CreatedAt time.Time `pg:"default:now()" json:"created_at"`
	UpdatedAt time.Time `pg:"default:now()" json:"updated_at"`
}
