package models

import (
	"go-qna-engine-sbercode-spring-2021/internal/generated/models"
	"time"
)

type Question struct {
	tableName           struct{}                      `pg:"question"`
	ID                  uint64                        `pg:",pk" json:"id"`
	CreatedBy           string                        `json:"created_by"`
	Header              string                        `json:"header"`
	Content             []byte                        `json:"content"`
	Priority            uint64                        `pg:"default:0" json:"priority"`
	Tags                string                        `json:"tags"`
	Rating              int64                         `json:"rating" pg:"default:0"`
	IsSolved            bool                          `pg:"default:false" json:"is_solved"`
	CreatedAt           time.Time                     `pg:"default:now()" json:"created_at"`
	UpdatedAt           time.Time                     `pg:"default:now()" json:"updated_at"`
	Answers             []*Answer                     `pg:"rel:has-many" json:"-"`
	AnswersWeb          []*models.AnswerInfo          `pg:"-" json:"-"`
	QuestionComments    []*QuestionComment            `pg:"rel:has-many" json:"-"`
	QuestionCommentsWeb []*models.QuestionCommentInfo `pg:"-" json:"-"`
}
