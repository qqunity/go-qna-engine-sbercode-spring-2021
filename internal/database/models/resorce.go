package models

type Resource struct {
	tableName  struct{} `pg:"resource"`
	ID         uint64   `pg:",pk" json:"id"`
	Identifier string   `json:"identifier"`
	Content    []byte   `json:"content"`
}
