module go-qna-engine-sbercode-spring-2021

go 1.15

require (
	github.com/DataDog/sketches-go v0.0.0-20190923095040-43f19ad77ff7 // indirect
	github.com/benbjohnson/clock v1.0.3 // indirect
	github.com/go-openapi/errors v0.20.0
	github.com/go-openapi/loads v0.20.2
	github.com/go-openapi/runtime v0.19.26
	github.com/go-openapi/spec v0.20.3
	github.com/go-openapi/strfmt v0.20.0
	github.com/go-openapi/swag v0.19.14
	github.com/go-openapi/validate v0.20.1
	github.com/go-pg/pg/v10 v10.7.7
	github.com/go-pg/pg/v9 v9.1.6 // indirect
	github.com/go-pg/urlstruct v0.4.0 // indirect
	github.com/google/gofuzz v1.0.0 // indirect
	github.com/jessevdk/go-flags v1.4.0
	github.com/segmentio/encoding v0.1.14 // indirect
	github.com/vmihailenco/msgpack/v4 v4.3.11 // indirect
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/grpc v1.30.0 // indirect
)
